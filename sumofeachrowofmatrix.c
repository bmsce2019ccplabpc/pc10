#include <stdio.h>
int main()
{
    int m,n;
    printf("enter size of matrix");
    scanf("%d%d",&m,&n);
    int A[m][n];
    int row, col, sum = 0;
    printf("Enter elements in matrix of size %dx%d: \n", m, n);
    for(row=0; row<m; row++)
    {
        for(col=0; col<n; col++)
        {
            scanf("%d", &A[row][col]);
        }
    }
    /* Calculate sum of elements of each row of matrix */
    for(row=0; row<m; row++)
    {
        sum = 0;
        for(col=0; col<n; col++)
        {
            sum += A[row][col];
        }

        printf("Sum of elements of Row %d = %d\n", row+1, sum);
    }    
    for(row=0; row<m; row++)
    {
        sum = 0;
        for(col=0; col<n; col++)
        {
            sum += A[col][row];
        }
        printf("Sum of elements of Column %d = %d\n", row+1, sum);
    }
    return 0;
}
